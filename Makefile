CC=gcc

# C flags
# with all warnings, with extra warnings

CFLAGS= -DDEBUG -Wall -Wextra -std=c11 -g 
# C preprocess flags
CPPFLAGS = 

# -Wl,(XX) means send xx to linker
# -Wl,--gc-sections means send --gc-sections to linker
LDFLAGS=-Wl,--gc-sections -g

# the first target in makefile is the default target
# which means
# `make` is the same with `make all`

# target: dependencies
# the next line of target starts with tab

executable_name := main

TOP  := $(shell pwd)/
SRC := .
all_source_files = $(sort $(shell find $(SRC) -name "*.c"))

$(executable_name): $(all_source_files:.c=.o)
	$(CC) $(LDFLAGS) $^ -o $(executable_name)


# $@ target
# $< first prerequisite
# $D the directory part of $@
# $^ The names of all the prerequisites, with spaces between them
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html

-include $(all_source_files:.c=.d)

%.d: %.c
	set -e; rm -f $@; \
	$(CC) -MM $(CPPFLAGS) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$


# A phony target is one that is not really the name of a file
.PHONY: clean
clean:
	rm -f $(executable_name)
	rm -f *.o
	rm -f *.d
