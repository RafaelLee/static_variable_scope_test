#include <stdio.h>

static int a;

void fun1()
{
  printf ("in fun1, before declare &a = 0x%lX, a = %d\n", (unsigned int *)&a, a);
  static int a;
  a += 1000;
  printf ("in fun1, end            &a = 0x%lX, a = %d\n", (unsigned int *)&a, a);
}

void fun0()
{
  printf ("in fun0, before declare &a = 0x%lX, a = %d\n", (unsigned int *)&a, a);
  static int a;
  a += 100;
  printf ("in fun0, end            &a = 0x%lX, a = %d\n", (unsigned int *)&a, a);
}


int main (void)
{
  a = 0;
  printf ("in main,                &a = 0x%lX, a = %d\n", (unsigned int *)&a, a) ;
  a += 3;
  printf ("in main, after add 3    &a = 0x%lX, a = %d\n", (unsigned int *)&a, a) ;
  printf ("------------\n");
  fun0();
  fun1();
  printf ("------------\n");
  fun0();
  fun1();
  printf ("------------\n");
  fun0();
  fun1();
  return 0;
}
